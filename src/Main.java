import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by nurlan on 29/05/16.
 */
public class Main {

    public static void main(String args[]) throws Exception {
        List<String> inputList = getStringFromFile(new FileReader("input.txt"));
        List<String> patternsList = getStringFromFile(new FileReader("patterns.txt"));


        System.out.println("mode1: " + matchModeOne(inputList, patternsList));
        System.out.println("mode2: " + matchModeTwo(inputList, patternsList));
        System.out.println("mode3: " + matchModeThree(inputList, patternsList));


    }

    public static List<String> matchModeOne(List<String> inputList, List<String> patternsList) {
        List<String> result = new ArrayList<String>();

        for(String input: inputList) {
            for(String pattern: patternsList) {
                if(input.equals(pattern)) {
                    result.add(input);
                }
            }
        }

        return result;
    }


    public static List<String> matchModeTwo(List<String> inputList, List<String> patternsList) {
        List<String> result = new ArrayList<String>();

        for(String input: inputList) {
            for(String pattern: patternsList) {
                if(input.contains(pattern)){
                    result.add(input);
                }
            }
        }

        return result;
    }

    public static List<String> matchModeThree(List<String> inputList, List<String> patternList) {
        List<String> result = new ArrayList<String>();

        for(String input: inputList) {
            for(String pattern: patternList) {
                if(input.equals(pattern) || isEditDistanceOne(input, pattern)) {
                    result.add(input);
                }
            }
        }

        return result;
    }


    public static boolean isEditDistanceOne(String input, String pattern) {
        int inputLength = input.length();
        int patternLength = pattern.length();

        if(Math.abs(inputLength - patternLength) > 1) {
            return false;
        }

        int count = 0;

        int i = 0;
        int j = 0;

        while(i < inputLength && j < patternLength) {
            if(input.charAt(i) != pattern.charAt(j)) {
                if(count == 1) {
                    return false;
                }

                if(inputLength > patternLength) {
                    i++;
                }
                else if(patternLength > inputLength) {
                    j++;
                }
                else {
                    i++;
                    j++;
                }

                count++;
            }
            else {
                i++;
                j++;
            }
        }

        if(i < inputLength || j < patternLength) {
            count++;
        }

        return count == 1;
    }

    public static List<String> getStringFromFile(FileReader file) throws Exception {
        BufferedReader bufferedReader = new BufferedReader(file);
        List<String> result = new ArrayList<String>();

        try {
            String line = bufferedReader.readLine();

            while(line != null) {
                result.add(line);
                line = bufferedReader.readLine();
            }

        } finally {
            bufferedReader.close();
        }

        return result;
    }
}
